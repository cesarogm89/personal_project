import os
import sys
import psutil 
#import matplotlib.pyplot as plt
from datetime import date
from datetime import datetime

# con esta funcion puedes saber si el archivo ya existe
# y si no lo crea con la fecha
def file_exist(ruta):
 fileName = r""+ruta
 if(os.path.exists(fileName)):
    calculate_memory_use(ruta)
 else:
   archivo = open(ruta, "w")
   archivo.close()

def calculate_memory_use(ruta):
 now = datetime.now()
 hora = str(now.hour) + ":" +str(now.minute)
 ram_usage = psutil.virtual_memory()[2]
# print('RAM memory % used:', ram_usage , 'Hora obtenida', hora, fecha) 
 archivo = open(ruta, "a")
 archivo.write(str(ram_usage)+ ","+ hora + ","+ fecha+"\n")
 archivo.close()

def calculate_cpu_use():
 print('The CPU usage is: ', psutil.cpu_percent(10)) 

today = date.today()
fecha = str(today.day) + "/"+str(today.month)+"/" +str(today.year)
ruta = "C:/MemoryLogs/"+ "memorylog.txt"
ruta2 = "C:/MemoryLogs/"+ "cpulog.txt"
file_exist(ruta)
#calculate_cpu_use()
sys.exit()