﻿// See https://aka.ms/new-console-template for more information
using System;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.IO;
class Program
{
    static void Main(string[] args)
    {
        int x=0;
        Console.WriteLine("Ingresa la IP del servidor: \n");
        string ipAddress = Console.ReadLine();
        Console.WriteLine("Ingresa el port: \n");
        int port = Convert.ToInt32(Console.ReadLine());
        do
        {
            Console.WriteLine("Ingresa el mensaje: \n");
            string mensaje = Console.ReadLine();
            TcpClient client = new TcpClient(ipAddress, port);
            try
            {
                // Establecer la dirección IP y el número de puerto del servidor al que se va a conectar
                // Crear un objeto TcpClient
                // Codificar el mensaje en bytes
                byte[] data = Encoding.ASCII.GetBytes(mensaje);
                // Obtener la secuencia de salida del cliente
                NetworkStream stream = client.GetStream();
                // Enviar el mensaje al servidor
                stream.Write(data, 0, data.Length);
                // Cerrar la conexión
                byte[] buffer = new byte[1024];
                int bytesRead = stream.Read(buffer, 0, buffer.Length);
                string response = Encoding.ASCII.GetString(buffer, 0, bytesRead);
                Console.WriteLine("Respuesta del servidor: " + response);
                Console.WriteLine("Deseas cerrar la conexio? si[0] no[1]");
                int anwers = Convert.ToInt32(Console.ReadLine());
                if (anwers==0)
                {
                    stream.Close();
                    client.Close();
                }
                else
                {
                    x = 1;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
        }
        while (x != 0);
    }
}
